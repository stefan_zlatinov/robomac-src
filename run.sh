#!/bin/bash

# Get the absolute directory path of the script
project_dir=$(dirname "$(readlink -f "$0")")

# Spin a container
podman run -it --rm -p 8888:8888 -v "$project_dir":/app:Z src_image

